package utils;

import models.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


/*
В этом классе мы создаем новый объект конфигураций Configuration,
 и передаем ему те классы, которые он должен воспринимать как сущности — User и Auto.
У него всего одна задача — создавать фабрику сессий для работы с БД (привет, паттерн "Фабрика!").
 */
public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;


    private HibernateSessionFactoryUtil() {}


    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(User.class);
                configuration.addAnnotatedClass(Auto.class);
                StandardServiceRegistryBuilder builder =
                        new StandardServiceRegistryBuilder()/*.configure("hibernate.cfg.xml");*/
                                .applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Exception e) {
                System.out.println("Исключение!" + e + "_____");
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}

/*
Hibernate.cfg.xml зачитывается здесь: new Configuration().configure();
Properties — это параметры для работы hibernate, указанные в специальном файле hibernate.cfg.xml.
В нем — параметры соединения с БД, и специальный параметр show_sql. Он нужен для того,
 чтобы все sql-запросы, которые hibernate будет выполнять к нашей БД, выводились в консоль.
  Таким образом, вы будете видеть что конкретно делает Hibernate в каждый момент времени.
 */