package models;

import javax.persistence.*;


/*
Чтобы класс мог быть сущностью, к нему предъявляются следующие требования:
 - Должен иметь пустой конструктор (public или protected);
 - Не может быть вложенным, интерфейсом или enum;
 - Не может быть final и не может содержать final-полей/свойств;
 - Должен содержать хотя бы одно @Id-поле.

При этом entity может:
 - Содержать непустые конструкторы;
 - Наследоваться и быть наследованным;
 - Содержать другие методы и реализовывать интерфейсы.

Нам понадобятся модели данных — классыUser и Auto.
 */
@Entity   //  @Entity (сущность) позволяет Java-объектам вашего класса быть связанными с БД.
@Table(name = "autos")
public class Auto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column (name = "model")
    private String model;

    //можно не указывать Column name, если оно совпадает с названием столбца в таблице
    private String color;

    /*
    @ManyToOne (многим Auto может соответствовать один User) и
    @JoinColumn. Она указывает, через какой столбец в таблице autos
     происходит связь с таблицей users (внешний ключ).
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;


    public Auto() {
    }
    public Auto(String model, String color) {
        this.model = model;
        this.color = color;
    }


    public int getId() {
        return id;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return color + " " + model;
    }
}